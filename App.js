import React from 'react';
import moment from 'moment';
import AlarmDisplay from 'AlarmDisplay';
import { StyleSheet, Text, View, TimePickerAndroid, Slider } from 'react-native';

export default class App extends React.Component {
  constructor(props){
    super(props)
    this.changeAlarmTime = this.changeAlarmTime.bind(this)

    this.state = {
      alarmTime: {hour: 7, minute: 0}
    }
  }

  async changeAlarmTime() {
    try {
      const {action, hour, minute} = await TimePickerAndroid.open({
        hour: 14,
        minute: 0,
        is24Hour: true,
      });
      if (action !== TimePickerAndroid.dismissedAction) {
        this.setState({alarmTime: {hour, minute}})
      }
    } catch({code, message}) {
      console.warn('Cannot open time picker', message)
    }
  }

  render() {
    const {alarmTime} = this.state
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Morning Hue</Text>
        <AlarmDisplay onClick={this.changeAlarmTime} time={alarmTime}/>
        <Slider style={styles.slider} minimumValue={-100} maximumValue={0}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
  }
});
