import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';

export default function AlarmDisplay(props) {
  const {time, onClick} = props;

  const toDoubleDigit = (i) => {
    if (i < 10){
      return "0"+i 
    } else {
      return ""+i;
    }
  }

  return (
    <View style={styles.container}>
      <TouchableHighlight onPress={onClick}>
        <View style={styles.AlarmDisplay}>
          <Text style={styles.text}>
          {toDoubleDigit(time.hour) + ':' + toDoubleDigit(time.minute)}
          </Text>
        </View>
      </TouchableHighlight>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  AlarmDisplay: {
    flexDirection: 'row',
  },
  text: {
    fontSize: 40,
  }
});
